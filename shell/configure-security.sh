#!/bin/bash

#Parameters check
if [ -z "$POSTGRES_DB" ]
then
    echo 'can'\''t configure slave server, $POSTGRES_DB parameter missing'
    exit 22
fi

if [ -z "$POSTGRES_USER" ]
then
    echo 'can'\''t configure slave server, $POSTGRES_USER parameter missing'
    exit 22
fi

#Make postgres to listen all addresses
sed -i -e "s/^#listen_addresses =.*\$/listen_addresses = '*'/" $PGDATA/postgresql.conf

#Remove md5 authentication
sed -i -e "/host all all all md5/d" /var/lib/postgresql/data/pg_hba.conf

#Add trust authentication
echo "host    all    all    all    trust" >> $PGDATA/pg_hba.conf