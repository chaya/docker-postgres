#!/bin/bash
# get latest production dump and install it
# get flyway scripts and install them

if [ -z "$ARTIFACT_URL" ]
then
    echo 'can'\''t get original dump, $REPO_URL parameter missing'
    exit 22
fi

if [ -z "$ARTIFACT_USERNAME" ]
then
    echo 'can'\''t get original dump, $REPO_USERNAME parameter missing'
    exit 22
fi

if [ -z "$ARTIFACT_PASSWORD" ]
then
    echo 'can'\''t get original dump, $REPO_PASSWORD parameter missing'
    exit 22
fi

if [ -z "$POSTGRES_DB" ]
then
    echo '$POSTGRES_DB parameter missing'
    exit 22
fi

if [ -z "$POSTGRES_USER" ]
then
    echo '$POSTGRES_USER parameter missing'
    exit 22
fi

DUMP_PATH=/tmp/bdd.dump

if [ -e "$DUMP_PATH" ]; then
    echo 'Dump already restored, skipping restoration'
else
    echo "ARTIFACT_URL: $ARTIFACT_URL"

	AUTHORIZATION=""
	if [ -z "$ARTIFACT_USERNAME" ]; then
		echo 'No credentials found'
	else
		echo 'Using credentials ' ${ARTIFACT_USERNAME}':*'
		AUTHORIZATION="-u $ARTIFACT_USERNAME:$ARTIFACT_PASSWORD"
	fi

	echo 'Downloading artifact at' ${ARTIFACT_URL} 'using authorization:' ${AUTHORIZATION}
    set -x
	curl -kvf $AUTHORIZATION $ARTIFACT_URL --output $DUMP_PATH
    set +x

    echo 'Restoring dump'
	set +e
	pg_restore -d ${POSTGRES_DB} -U ${POSTGRES_USER} -j8  -Fc "$DUMP_PATH"
	set -e
fi