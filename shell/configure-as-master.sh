#!/bin/bash

#Parameters check
if [ -z "$POSTGRES_USER" ]
then
    echo 'can'\''t configure slave server, $POSTGRES_USER parameter missing'
    exit 22
fi

if [ -z "$CLUSTER_REPLICATION_USER" ]
then
    echo 'can'\''t configure slave server, $CLUSTER_REPLICATION_USER parameter missing'
    exit 22
fi

if [ -z "$CLUSTER_REPLICATION_PASSWORD" ]
then
    echo 'can'\''t configure slave server, $CLUSTER_REPLICATION_PASSWORD parameter missing'
    exit 22
fi

WAL_MAX_SENDERS=${CLUSTER_WAL_SENDERS:=1}
WAL_KEEP_SEGMENTS=${CLUSTER_WAL_KEEP_SEGMENTS:=64}

echo "Setting CLUSTER_WAL_SENDERS to $WAL_MAX_SENDERS"
echo "Setting CLUSTER_WAL_KEEP_SEGMENTS to $WAL_KEEP_SEGMENTS"

cat >> ${PGDATA}/postgresql.conf <<EOF
	wal_level = hot_standby
	max_wal_senders = $WAL_MAX_SENDERS
	wal_keep_segments = $WAL_KEEP_SEGMENTS
	hot_standby = on
EOF

#Allow slaves connection to master for replication
echo "host    replication    all    all    trust" >> $PGDATA/pg_hba.conf

#Create an user for replication
#  - SUPERUSER (to check in there is a backup running on master)
#  - REPLICATION (to allow WAL access)
#  - LOGIN (to allow incoming connection)
psql --username "$POSTGRES_USER" -c "CREATE USER $CLUSTER_REPLICATION_USER SUPERUSER REPLICATION LOGIN CONNECTION LIMIT -1 ENCRYPTED PASSWORD '$CLUSTER_REPLICATION_PASSWORD';"