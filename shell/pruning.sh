#!/bin/bash
#prune the production dump and push it to artifactory

if [ -z "$TARGET_REPOSITORY" ]
then
    echo '$TARGET_REPOSITORY parameter missing'
    exit 22
fi

if [ -z "$ARTIFACT_USERNAME" ]
then
    echo 'can'\''t get original dump, $ARTIFACT_USERNAME parameter missing'
    exit 22
fi

if [ -z "$ARTIFACT_PASSWORD" ]
then
    echo 'can'\''t get original dump, $ARTIFACT_PASSWORD parameter missing'
    exit 22
fi

if [ -z "$POSTGRES_DB" ]
then
    echo '$POSTGRES_DB parameter missing'
    exit 22
fi

if [ -z "$POSTGRES_USER" ]
then
    echo '$POSTGRES_USER parameter missing'
    exit 22
fi

if [ -z "$PRUNING_SQL_FILE" ]
then
	echo "No pruning file \$PRUNING_SQL_FILE=$PRUNING_SQL_FILE"
	exit 22
fi

if [ -z "$BDD_GIT_REPOSITORY" ]
then
    echo '$BDD_GIT_REPOSITORY parameter missing'
    exit 22
fi

if [ -z "$PRUNING_GIT_REPOSITORY" ]
then
    echo '$PRUNING_GIT_REPOSITORY parameter missing'
    exit 22
fi

if [ -z "$BDD_GIT_BRANCH" ]
then
    echo '$BDD_GIT_BRANCH parameter missing'
    exit 22
fi

if [ -z "$BDD_SCHEMA_VERSION" ]
then
    echo '$SCHEMA_VERSION not set, setting it by default to schema_version'
    BDD_SCHEMA_VERSION='schema_version'
fi

#Download flyway bdd data
mkdir /tmp/project
cd /tmp/project
git init
git remote add origin ${BDD_GIT_REPOSITORY}
git config core.sparseCheckout true
if [ -z "$BDD_GIT_FLYWAY_DIRECTORY" ]
then
	echo "No flyway directory found \$BDD_GIT_FLYWAY_DIRECTORY=$BDD_GIT_FLYWAY_DIRECTORY"
else
	echo ${BDD_GIT_FLYWAY_DIRECTORY} >> .git/info/sparse-checkout
fi
echo 'Downloading flyway data...'
git pull origin ${BDD_GIT_BRANCH}

#Migrate all script under $BDD_GIT_FLYWAY_DIRECTORY
if [ -z "$BDD_GIT_FLYWAY_DIRECTORY" ]
then
	echo "No flyway scripts availables, skipping flyway migration"
else
    echo 'Migrating flyway scripts'
	/flyway-bin/flyway \
		-url=jdbc:postgresql://localhost/${POSTGRES_DB} \
		-driver=org.postgresql.Driver \
		-schemas=postgres \
		-table=${BDD_SCHEMA_VERSION} \
		-user=${POSTGRES_USER} \
		-password=${POSTGRES_PASSWORD} \
		-baselineOnMigrate=false \
		-locations='filesystem:/tmp/project/'${BDD_GIT_FLYWAY_DIRECTORY} \
		migrate
fi

#Prune the dump
mkdir /tmp/pruning
cd /tmp/pruning
git init
git remote add origin ${PRUNING_GIT_REPOSITORY}
git config core.sparseCheckout true
echo ${PRUNING_SQL_FILE} >> .git/info/sparse-checkout
echo pom.xml >> .git/info/sparse-checkout

echo 'Downloading pruning file...'
git pull origin ${BDD_GIT_BRANCH}

if [ -f "/tmp/pruning/$PRUNING_SQL_FILE" ]; then
    echo "pruning the dump ..."
    set +e
    psql -d ${POSTGRES_DB} -U ${POSTGRES_USER} < /tmp/pruning/$PRUNING_SQL_FILE
    set -e
else
    echo 'Failed to determine pruning script'
    exit 22
fi

echo 'creating new dump ...'
set +e
pg_dump -U ${POSTGRES_USER} -Fc $POSTGRES_DB > "/tmp/newDump.dump"
set -e

#java -XshowSettings:properties -version
#echo "JAVA_HOME $JAVA_HOME"
#java -version
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre"

FILE_CHECKSUM=$(shasum -a 1 "/tmp/newDump.dump" | awk '{ print $1 }') 

MVN_VERSION=$(mvn -q \
    -Dexec.executable=echo \
    -Dexec.args='${project.version}' \
    --non-recursive \
    exec:exec 2>/dev/null)

MVN_GROUP=$(mvn -q \
    -Dexec.executable=echo \
    -Dexec.args='${project.groupId}' \
    --non-recursive \
    exec:exec 2>/dev/null)
MVN_GROUP=$(echo "$MVN_GROUP" | sed 's,\.,/,g')

MVN_ARTIFACT=$(mvn -q \
    -Dexec.executable=echo \
    -Dexec.args='${project.artifactId}' \
    --non-recursive \
    exec:exec 2>/dev/null)

TIMESTAMP=`date '+%Y%m%d.%H%M%S'`

AUTHORIZATION=""
if [ -z "$ARTIFACT_USERNAME" ]; then
    echo 'No credentials found'
else
    echo 'Using credentials ' ${ARTIFACT_USERNAME}':*'
    AUTHORIZATION="-u $ARTIFACT_USERNAME:$ARTIFACT_PASSWORD"
fi

echo "sending ${MVN_ARTIFACT}-${BDD_GIT_BRANCH}.dump to repository ..."

set -x
curl -v $AUTHORIZATION -X POST -F "raw.directory=${MVN_GROUP}/${MVN_ARTIFACT}/${BDD_GIT_BRANCH}-SNAPSHOT" -F "raw.asset1=@/tmp/newDump.dump" -F "raw.asset1.filename=${MVN_ARTIFACT}-${BDD_GIT_BRANCH}-${TIMESTAMP}.dump" "$TARGET_REPOSITORY"
set +x