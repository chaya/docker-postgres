#!/bin/bash

#Parameters check
if [ -z "$CLUSTER_MASTER_HOST" ]
then
    echo 'can'\''t configure slave server, $CLUSTER_MASTER_HOST parameter missing'
    exit 22
fi

if [ -z "$CLUSTER_REPLICATION_USER" ]
then
    echo 'can'\''t configure slave server, $CLUSTER_REPLICATION_USER parameter missing'
    exit 22
fi

if [ -z "$CLUSTER_REPLICATION_PASSWORD" ]
then
    echo 'can'\''t configure slave server, $CLUSTER_REPLICATION_PASSWORD parameter missing'
    exit 22
fi

#When exiting recovery mode, the recovery.conf file is renamed to recovery.done, so that a subsequent restart goes directly to normal mode
#To manually exit recovery mode, you can create a trigger file specified in the recovery.conf
cat > ${PGDATA}/recovery.conf <<EOF
	standby_mode = on
	primary_conninfo = 'host=$CLUSTER_MASTER_HOST port=5432 user=$CLUSTER_REPLICATION_USER password=$CLUSTER_REPLICATION_PASSWORD sslmode=prefer'
	trigger_file = '$PGDATA/tmp/create_me_to_stop_recovery_mode.trigger'
EOF

chown postgres ${PGDATA}/recovery.conf
chmod 600 ${PGDATA}/recovery.conf

#Allow master connection to slave for replication (in case of failover)
echo "host    replication    all    $CLUSTER_MASTER_HOST    trust" >> $PGDATA/pg_hba.conf