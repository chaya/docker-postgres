#!/bin/bash

if [ -z "$MODE" ]
then
    echo "no MODE detected, setting default MODE to START"
    MODE="START"
else
    echo "current MODE is $MODE"
fi

sh /opt/configure-security.sh

if [ "$CLUSTER_NODE_TYPE" = "MASTER" ]
then
    echo 'Setup database as master server for cluster'
    sh /opt/configure-as-master.sh
elif [ "$CLUSTER_NODE_TYPE" = "SLAVE" ]
then
    echo 'Setup database as slave server for cluster'
    sh /opt/configure-as-slave.sh
else
    echo 'Setup database as standalone server'
fi

if [ -n "$ARTIFACT_URL" ] &&  [ "$CLUSTER_NODE_TYPE" != "SLAVE" ]
then
    echo 'Installing database'
    bash /opt/database-install.sh
fi

if [ "$MODE" = "DUMP" ]
then
    sh /opt/pruning.sh
fi