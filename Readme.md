#Description
This image run classic postgres image, and allow you to do some additionnal operations:
* MODE=START (default)
  * Install database with dump downloaded from artifactory/newus.
  * Configure the database as master or slave server for clustering
* MODE=DUMP
  * Install database with dump downloaded from artifactory/newus.
  * Migrate database with source code downloaded from git repo. (via Flyway)
  * Apply a pruning SQL script
  * Create a new dump and deploy it to artifactory based on PRUNING_GIT_REPOSITORY pom infos

#Basic configuration:
* ARTIFACT_URL=http://mynexus.com/path/to/my/artifact
* ARTIFACT_USERNAME=user
* ARTIFACT_PASSWORD=password
* POSTGRES_DB=database-name
* POSTGRES_USER=username
* POSTGRES_PASSWORD=password
* MODE=START/DUMP (optional, default=START)

#Dump management
* BDD_GIT_REPOSITORY=https://username:password@bitbucket.org/chaya/my-project.git
* PRUNING_GIT_REPOSITORY=https://username:password@bitbucket.org/chaya/my-project.git (optional, only for DUMP mode)
* BDD_GIT_BRANCH=develop
* BDD_GIT_FLYWAY_DIRECTORY=src/main/resources/db/flywayscripts
* BDD_SCHEMA_VERSION=schema_version (optionnal, table for flyway versioning)
* PRUNING_SQL_FILE= src/main/resources/prune.sql (optional, only for DUMP mode)
* TARGET_REPOSITORY=http://mynexus.com/service/rest/v1/components\?repository=raw-snapshots (where the dump will be push)

#Cluster master server (optionnal)
* CLUSTER_NODE_TYPE=MASTER (case sensitive)
* CLUSTER_REPLICATION_USER=replication
* CLUSTER_REPLICATION_PASSWORD=password
* CLUSTER_WAL_SENDERS=1 (default to 1)
* CLUSTER_WAL_KEEP_SEGMENTS=8 (default to 8)

#Cluster slave server (optionnal)
* CLUSTER_NODE_TYPE=SLAVE (case sensitive)
* CLUSTER_MASTER_HOST=master-hostname
* CLUSTER_REPLICATION_USER=replication
* CLUSTER_REPLICATION_PASSWORD=password